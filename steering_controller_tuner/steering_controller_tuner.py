#!/usr/bin/env python3
"""
Obstacle generator for
testing the embotech control interface. 
This will publish an obstacle when the ALV is within a certain distance of it.
"""
import rclpy
from rclpy.node import Node

import math
import random
import functools
import rclpy
from rclpy.node import Node
from autonomy_msgs.msg import Pose
from autonomy_msgs.msg import TranslationData
from proparker_msgs.msg import TrackerOutput
from proparker_msgs.msg import Trajectory
import numpy
from rclpy.qos import qos_profile_default, qos_profile_sensor_data
from rclpy.qos import QoSProfile
from rclpy.qos import QoSReliabilityPolicy
from rclpy.qos import QoSDurabilityPolicy

class SteeringControllerTuner(Node):                 # pylint: disable=too-many-instance-attributes
    """

    Publishes "/proparker/tracker" message type.
    """
    def __init__(self):
        super().__init__('steering_controller_tuner')
        result = self.declare_parameters(
            namespace='',
            parameters=[
                ('pose_sub_topic_name', None),
                ('command_pub_topic_name', None),
                ('longitudinal_command_pub_topic_name', None),
                ('command_pub_hz', None),
                ('step_test_enabled', None),
                ('sin_test_enabled', None),
                ('step_start_time', None),
                ('step_test_steer_rate_command', None),
                ('step_test_steer_pos_command', None),
                ('sin_steering_amplitude', None),
                ('sin_steering_rate_hz', None),
                ('start_engine_throttle_time', None),
                ('const_engine_throttle_command', None),
                ('end_test_time', None),
                ('ramp_test_enabled', None),
                ('ramp_start_time', None),
                ('ramp_rate_up_down', None),
                ('ramp_max_amplitude', None),
                ('ramp_down_time', None)
                ]
        )


        self.awl_x = None
        self.awl_y = None
        self.awl_heading = None
        self.pose = None

        # params
        self.start_engine_throttle_time = self.get_parameter('start_engine_throttle_time').value
        self.ramp_test_enabled = self.get_parameter('ramp_test_enabled').value
        self.ramp_start_time = self.get_parameter('ramp_start_time').value
        self.ramp_rate_up_down = self.get_parameter('ramp_rate_up_down').value
        self.ramp_max_amplitude = self.get_parameter('ramp_max_amplitude').value
        self.ramp_down_time = self.get_parameter('ramp_down_time').value
        self.longitudinal_command_pub_topic_name = self.get_parameter('longitudinal_command_pub_topic_name').value
        command_pub_name = self.get_parameter('command_pub_topic_name').value
        self.pose_sub_topic_name = self.get_parameter('pose_sub_topic_name').value        
        self.step_start_time = self.get_parameter('step_start_time').value
        self.step_test_steer_rate_command = self.get_parameter('step_test_steer_rate_command').value
        self.step_test_steer_pos_command = self.get_parameter('step_test_steer_pos_command').value
        self.sin_steering_amplitude = self.get_parameter('sin_steering_amplitude').value
        self.sin_steering_rate_hz = self.get_parameter('sin_steering_rate_hz').value
        self.const_engine_throttle_command = self.get_parameter('const_engine_throttle_command').value
        self.end_test_time = self.get_parameter('end_test_time').value
        self.step_test_enabled= self.get_parameter('step_test_enabled').value
        self.sin_test_enabled= self.get_parameter('sin_test_enabled').value
        #import ipdb; ipdb.set_trace()
        best_eff_qos_profile = QoSProfile(
            depth=1,
            reliability=QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT,
            durability=QoSDurabilityPolicy.RMW_QOS_POLICY_DURABILITY_VOLATILE
        )

        self.embotech_pub = self.create_publisher(TrackerOutput, 
                                                  command_pub_name,
                                                  best_eff_qos_profile)
        
        self.pose_sub_callback = self.create_subscription(Pose, 
                            self.pose_sub_topic_name, 
                            self.pose_callback, 
                            best_eff_qos_profile)

        self.longitudinal_pub = self.create_publisher(TranslationData, 
                                                  self.longitudinal_command_pub_topic_name,
                                                  best_eff_qos_profile)

        self.id_counter = 0

        # timer based publisher
        pub_hz = self.get_parameter('command_pub_hz').value
        if pub_hz > 0.0:
            self.timer_period = 1.0 / float(pub_hz)         # seconds
        else:
            self.timer_period = 0.033         # seconds
        self.timer = self.create_timer(self.timer_period, self.timer_callback)

        
        self.frame_id = "map"
        self.latest_received_msg_timestamp = None

        self.idx = 0 # Internal incrementing counter
        self.time = 0.0 # INternal timer since start in secs.

        self.desired_steering_angle_ = 0.0
        self.desired_steering_rate_ = 0.15  # rad/s

        self.command_msg = TrackerOutput()
        self.longitudinal_command_msg = TranslationData()

        self.num_predictions = 1000
        # Set tracker timestamps
        self.set_trajectory_times()
        
        self.command_msg.trajectory.num_elements = self.num_predictions

        self.longitudinal_command_msg.engine_demand.unit.unit = 10 # 0 to 100 scaled
        self.longitudinal_command_msg.engine_demand.minimum = 0.0
        self.longitudinal_command_msg.engine_demand.minimum = 100.0
        self.longitudinal_command_msg.service_brake.unit.unit = 10 # 0 to 100 scaled
        self.longitudinal_command_msg.service_brake.minimum = 0.0
        self.longitudinal_command_msg.service_brake.minimum = 100.0
        self.longitudinal_command_msg.is_supported = True
        self.longitudinal_command_msg.engine_demand.is_supported = True
        self.longitudinal_command_msg.service_brake.is_supported = True
        self.longitudinal_command_msg.engine_demand.is_valid = True
        self.longitudinal_command_msg.service_brake.is_valid = True

        self.longitudinal_command_msg.transmission.prnd_mode.is_supported = True
        self.longitudinal_command_msg.transmission.prnd_mode.is_valid = True
        self.longitudinal_command_msg.transmission.prnd_mode.minimum = 0
        self.longitudinal_command_msg.transmission.prnd_mode.maximum = 4
        self.longitudinal_command_msg.transmission.prnd_mode.unit.unit = 0 # NO UNIT 

        self.longitudinal_command_msg.transmission.prnd_mode.value = 4 # FORWARDS GEAR
           
    def set_trajectory_times(self):
        for i in range(self.num_predictions):
            self.command_msg.trajectory.time[i] = i * 0.1

    def set_desired_steering_command(self):
        if (self.time < self.end_test_time and self.time > self.start_engine_throttle_time):
            for i in range(self.num_predictions):
                self.command_msg.trajectory.steer_angle[i] = self.desired_steering_angle_
                self.command_msg.trajectory.steer_rate[i] = self.desired_steering_rate_
        else:
            for i in range(self.num_predictions):
                self.desired_steering_angle_ = 0.0
                self.desired_steering_rate_ = 0.0
                self.command_msg.trajectory.steer_angle[i] = 0.0
                self.command_msg.trajectory.steer_rate[i] = 0.0

    def set_pred_positions(self):
        for i in range(self.num_predictions):
            self.command_msg.trajectory.x_rear_axle[i] = self.awl_x
            self.command_msg.trajectory.y_rear_axle[i] = self.awl_y
            self.command_msg.trajectory.heading[i] = self.awl_heading
            # Just set the predicted velocity to the current value.
            if (self.pose is not None):
                self.command_msg.trajectory.vel[i] = self.pose.vel_x

    def set_pred_longitudinal_commands(self):
        
        if (self.time < self.end_test_time and self.time > self.start_engine_throttle_time):

            self.longitudinal_command_msg.engine_demand.value = self.const_engine_throttle_command
            self.longitudinal_command_msg.service_brake.value = 0.0
        else:
            self.longitudinal_command_msg.engine_demand.value = 0.0
            self.longitudinal_command_msg.service_brake.value = 50.0

    def pose_callback(self, pose_msg):
        """
        Updates the position of the ALV
        """
        self.awl_x = pose_msg.easting
        self.awl_y = pose_msg.northing
        self.awl_heading = pose_msg.yaw
        self.latest_pose_msg = pose_msg
        self.latest_received_msg_timestamp = pose_msg.header.stamp
        #print("self.latest_received_msg_timestamp = ", self.latest_received_msg_timestamp) 

    def sin_test(self):
        #import ipdb; ipdb.set_trace()
        if (self.time < self.end_test_time and self.time > self.start_engine_throttle_time):
            new_steer = self.sin_steering_amplitude * math.sin( (self.time - self.start_engine_throttle_time) * 2.0 * math.pi * self.sin_steering_rate_hz )
            self.desired_steering_angle_ = new_steer
            self.desired_steering_rate_ = 2.0 * math.pi * self.sin_steering_rate_hz * self.sin_steering_amplitude * math.cos( (self.time - self.start_engine_throttle_time) * 2.0 * math.pi * self.sin_steering_rate_hz )
        else:
            self.desired_steering_angle_ = 0.0
            self.desired_steering_rate_ = 0.0


    def step_test(self):
        if (self.time > self.step_start_time):
            new_steer = self.step_test_steer_pos_command
            self.desired_steering_angle_ = new_steer
            self.desired_steering_rate_ = self.step_test_steer_rate_command

    def ramp_test(self):
        if (self.time > self.ramp_start_time and self.time > self.start_engine_throttle_time):
            
            # Ramp Up
            if (self.time < self.ramp_down_time):
                ramp_step = self.ramp_rate_up_down * self.timer_period
                self.desired_steering_angle_ = (self.desired_steering_angle_ + ramp_step)
                self.desired_steering_rate_ = self.ramp_rate_up_down

            # Hold maximum value.
            if (self.desired_steering_angle_ > self.ramp_max_amplitude):
                self.desired_steering_angle_ = self.ramp_max_amplitude
                self.desired_steering_rate_ = 0.0

            # Ramp down
            if (self.time > self.ramp_down_time):
                ramp_step = - self.ramp_rate_up_down * self.timer_period
                self.desired_steering_angle_ = (self.desired_steering_angle_ + ramp_step)
                self.desired_steering_rate_ =  - self.ramp_rate_up_down

            # Hold minimum value.
            if (self.desired_steering_angle_ < 0.0):
                self.desired_steering_angle_ = 0.0
                self.desired_steering_rate_ = 0.0


    def timer_callback(self):
          
        self.idx += 1
        self.time = self.idx * self.timer_period

        if self.sin_test_enabled:
            self.sin_test()

        if self.step_test_enabled:
            self.step_test()

        if self.ramp_test_enabled:
            self.ramp_test()
        

        #self.command_msg
        self.command_msg.header.stamp = self.get_clock().now().to_msg()
        self.command_msg.header.frame_id = self.frame_id
        self.command_msg.trajectory.timestamp = float(self.command_msg.header.stamp.sec + 1e-9*self.command_msg.header.stamp.nanosec)

        self.longitudinal_command_msg.timestamp.seconds = self.command_msg.header.stamp.sec
        self.longitudinal_command_msg.timestamp.nanos = self.command_msg.header.stamp.nanosec

        self.set_desired_steering_command()
        self.set_pred_positions()

        self.set_pred_longitudinal_commands()

        print("steer angle: ",self.desired_steering_angle_)
        print("steer rate: ",self.desired_steering_rate_)
        print("Engine demand: ", self.longitudinal_command_msg.engine_demand.value)
        print("Engine brake: ", self.longitudinal_command_msg.service_brake.value)
        
        self.embotech_pub.publish(self.command_msg)

        self.longitudinal_pub.publish(self.longitudinal_command_msg)

def main(args=None):
    rclpy.init(args=args)



    steer_tuner = SteeringControllerTuner()
    rclpy.spin(steer_tuner)

    steer_tuner.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()